/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
#include <stdio.h>
#include "nrf24_API.h"

char GlobalBuffer[10];
char display[8];
char payloadBuffer[9];


void spi_write(uint8* BufferArray, uint8 ByteCount)         //sequence for driving CS line & writing data over SPI
{
    SPI_CS_Write(0);                                        //pull select chip low for SPI communication
    CyDelay(1);                                             //guarantees 1ms delay between CS and SPI transmission
    SPIM_PutArray(BufferArray,ByteCount);
    while(!(SPIM_ReadTxStatus() & SPIM_STS_SPI_DONE));
    SPI_CS_Write(1);                                        //set select chip high to indicate SPI done
    CyDelay(1);
}

void set_reg_val(uint8 reg, uint8 val, uint8 num_bytes)     //function for setting buffer register
{
    uint8 Buffer[num_bytes];
    Buffer[0] = reg;
    for(int i = 1; i < num_bytes; i++) Buffer[i] = val;     //to send hex value one byte at a time via variable buffer size
    spi_write(Buffer,num_bytes);
}

void configure()
{
    set_reg_val(NRF_WRITE_SETUP_RETR,0xFF,2);   //access the setup register & sets wait to 4000us & number of retransmissions to 15
                    
    set_reg_val(NRF_WRITE_RF_CH,0x60,2);        //access the channel register and set the operating channel to 0x60
    
    set_reg_val(NRF_WRITE_RF_SETUP,0x26,2);     //access the RF setup register & set 250kbps & highest power settings
    
    set_reg_val(NRF_WRITE_TX_ADDR,0xC2,6);      //access transmit address register, have to send each 0xC2 byte separately. makes 0xC2C2C2C2C2 for transmit address
    
    set_reg_val(NRF_WRITE_RX_ADDR_P0,0xC2,6);   //access receiver address register & set receiver pipe 0 address to 0xC2C2C2C2C2
    
    set_reg_val(NRF_WRITE_RX_ADDR_P1,0xE7,6);   //access the receive address of the transmitter and set it to 0xE7E7E7E7E7
    
    set_reg_val(NRF_WRITE_PW_P0,0x08,2);        //access the data pipe 0 register and set number of payload bytes to 8
    
    set_reg_val(NRF_WRITE_PW_P1,0x08,2);        //access the data pipe 1 register and set number of payload bytes to 8
    
    set_reg_val(NRF_WRITE_EN_AA,0x03,2);        //access auto acknowledge register and turn on auto ack for data pipes 0 & 1
    
    set_reg_val(NRF_WRITE_EN_RXADDR,0x03,2);    //access enable rx address to enable data pipes 0 & 1
    
    set_reg_val(NRF_WRITE_CONFIG,0x0E,2);       //access config register and set 2 byte CRCs, power on radio, and set to transmit mode
}

void write_payload()
{
    payloadBuffer[0] = NRF_WRITE_PAYLOAD;
    for(int i = 1; i < 9; i++) payloadBuffer[i] = GlobalBuffer[i-1];    //filling the payloadBuffer with the data from the GlobalBuffer
    spi_write((uint8*)payloadBuffer, 9);                                //send payload command and then buffer with "I: YYY.Y"
    while(NRF24_IRQ_Read() != 0);                                       //poll IRQ pin until IRQ goes to 0 meaning that successful transmission occurred or max number of retries occurred
    set_reg_val(NRF_WRITE_STATUS,0x3E,2);                               //access status register and clear TX_DS & MAX_RT by writing 1s to them which is 0x3E
}

void get_temp()
{
    float voltage = (((float)(ADC_Read32()))/1048575.0)*2.048;  //gets the voltage level from the ADC with 20 bit resolution and 2.048V max
    float temp = voltage / 0.01;                                //voltage temperature conversion
    
    sprintf(GlobalBuffer,"I: %3.1f",temp);                      //puts the string in the global buffer with the temperature value to the nearest 0.1F
    sprintf(display,"%3.1f",temp);                              //rounds to 0.1 place
    LCD_Position(0,0);
    LCD_PrintString(display);                                   //displays the rounded temperature to the LCD
    LCD_PutChar(LCD_CUSTOM_0);
    LCD_PutChar('F');
    write_payload();
    CyDelay(1000);                                              //get the temp every 1 second
    LCD_ClearDisplay();
}

int main(void)
{
    CyGlobalIntEnable; /* Enable global interrupts. */
    /* Place your initialization/startup code here (e.g. MyInst_Start()) */
    NRF24_CE_Write(1);          //Set CE to 1 before sending commands via SPI to radio
    SPIM_Start();
    ADC_Start();
    ADC_StartConvert();         //start continuous ADC conversions
    LCD_Start();
    configure();                //configure the settings for the radio

    for(;;)
    {
        /* Place your application code here. */
        get_temp();                                 //calls ADC function to convert temperature & display it on LCD & output it to the radio via SPI
    }
}

/* [] END OF FILE */
