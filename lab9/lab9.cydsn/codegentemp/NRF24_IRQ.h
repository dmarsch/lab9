/*******************************************************************************
* File Name: NRF24_IRQ.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_NRF24_IRQ_H) /* Pins NRF24_IRQ_H */
#define CY_PINS_NRF24_IRQ_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "NRF24_IRQ_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 NRF24_IRQ__PORT == 15 && ((NRF24_IRQ__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    NRF24_IRQ_Write(uint8 value);
void    NRF24_IRQ_SetDriveMode(uint8 mode);
uint8   NRF24_IRQ_ReadDataReg(void);
uint8   NRF24_IRQ_Read(void);
void    NRF24_IRQ_SetInterruptMode(uint16 position, uint16 mode);
uint8   NRF24_IRQ_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the NRF24_IRQ_SetDriveMode() function.
     *  @{
     */
        #define NRF24_IRQ_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define NRF24_IRQ_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define NRF24_IRQ_DM_RES_UP          PIN_DM_RES_UP
        #define NRF24_IRQ_DM_RES_DWN         PIN_DM_RES_DWN
        #define NRF24_IRQ_DM_OD_LO           PIN_DM_OD_LO
        #define NRF24_IRQ_DM_OD_HI           PIN_DM_OD_HI
        #define NRF24_IRQ_DM_STRONG          PIN_DM_STRONG
        #define NRF24_IRQ_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define NRF24_IRQ_MASK               NRF24_IRQ__MASK
#define NRF24_IRQ_SHIFT              NRF24_IRQ__SHIFT
#define NRF24_IRQ_WIDTH              1u

/* Interrupt constants */
#if defined(NRF24_IRQ__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in NRF24_IRQ_SetInterruptMode() function.
     *  @{
     */
        #define NRF24_IRQ_INTR_NONE      (uint16)(0x0000u)
        #define NRF24_IRQ_INTR_RISING    (uint16)(0x0001u)
        #define NRF24_IRQ_INTR_FALLING   (uint16)(0x0002u)
        #define NRF24_IRQ_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define NRF24_IRQ_INTR_MASK      (0x01u) 
#endif /* (NRF24_IRQ__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define NRF24_IRQ_PS                     (* (reg8 *) NRF24_IRQ__PS)
/* Data Register */
#define NRF24_IRQ_DR                     (* (reg8 *) NRF24_IRQ__DR)
/* Port Number */
#define NRF24_IRQ_PRT_NUM                (* (reg8 *) NRF24_IRQ__PRT) 
/* Connect to Analog Globals */                                                  
#define NRF24_IRQ_AG                     (* (reg8 *) NRF24_IRQ__AG)                       
/* Analog MUX bux enable */
#define NRF24_IRQ_AMUX                   (* (reg8 *) NRF24_IRQ__AMUX) 
/* Bidirectional Enable */                                                        
#define NRF24_IRQ_BIE                    (* (reg8 *) NRF24_IRQ__BIE)
/* Bit-mask for Aliased Register Access */
#define NRF24_IRQ_BIT_MASK               (* (reg8 *) NRF24_IRQ__BIT_MASK)
/* Bypass Enable */
#define NRF24_IRQ_BYP                    (* (reg8 *) NRF24_IRQ__BYP)
/* Port wide control signals */                                                   
#define NRF24_IRQ_CTL                    (* (reg8 *) NRF24_IRQ__CTL)
/* Drive Modes */
#define NRF24_IRQ_DM0                    (* (reg8 *) NRF24_IRQ__DM0) 
#define NRF24_IRQ_DM1                    (* (reg8 *) NRF24_IRQ__DM1)
#define NRF24_IRQ_DM2                    (* (reg8 *) NRF24_IRQ__DM2) 
/* Input Buffer Disable Override */
#define NRF24_IRQ_INP_DIS                (* (reg8 *) NRF24_IRQ__INP_DIS)
/* LCD Common or Segment Drive */
#define NRF24_IRQ_LCD_COM_SEG            (* (reg8 *) NRF24_IRQ__LCD_COM_SEG)
/* Enable Segment LCD */
#define NRF24_IRQ_LCD_EN                 (* (reg8 *) NRF24_IRQ__LCD_EN)
/* Slew Rate Control */
#define NRF24_IRQ_SLW                    (* (reg8 *) NRF24_IRQ__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define NRF24_IRQ_PRTDSI__CAPS_SEL       (* (reg8 *) NRF24_IRQ__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define NRF24_IRQ_PRTDSI__DBL_SYNC_IN    (* (reg8 *) NRF24_IRQ__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define NRF24_IRQ_PRTDSI__OE_SEL0        (* (reg8 *) NRF24_IRQ__PRTDSI__OE_SEL0) 
#define NRF24_IRQ_PRTDSI__OE_SEL1        (* (reg8 *) NRF24_IRQ__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define NRF24_IRQ_PRTDSI__OUT_SEL0       (* (reg8 *) NRF24_IRQ__PRTDSI__OUT_SEL0) 
#define NRF24_IRQ_PRTDSI__OUT_SEL1       (* (reg8 *) NRF24_IRQ__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define NRF24_IRQ_PRTDSI__SYNC_OUT       (* (reg8 *) NRF24_IRQ__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(NRF24_IRQ__SIO_CFG)
    #define NRF24_IRQ_SIO_HYST_EN        (* (reg8 *) NRF24_IRQ__SIO_HYST_EN)
    #define NRF24_IRQ_SIO_REG_HIFREQ     (* (reg8 *) NRF24_IRQ__SIO_REG_HIFREQ)
    #define NRF24_IRQ_SIO_CFG            (* (reg8 *) NRF24_IRQ__SIO_CFG)
    #define NRF24_IRQ_SIO_DIFF           (* (reg8 *) NRF24_IRQ__SIO_DIFF)
#endif /* (NRF24_IRQ__SIO_CFG) */

/* Interrupt Registers */
#if defined(NRF24_IRQ__INTSTAT)
    #define NRF24_IRQ_INTSTAT            (* (reg8 *) NRF24_IRQ__INTSTAT)
    #define NRF24_IRQ_SNAP               (* (reg8 *) NRF24_IRQ__SNAP)
    
	#define NRF24_IRQ_0_INTTYPE_REG 		(* (reg8 *) NRF24_IRQ__0__INTTYPE)
#endif /* (NRF24_IRQ__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_NRF24_IRQ_H */


/* [] END OF FILE */
