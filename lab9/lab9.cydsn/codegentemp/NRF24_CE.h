/*******************************************************************************
* File Name: NRF24_CE.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_NRF24_CE_H) /* Pins NRF24_CE_H */
#define CY_PINS_NRF24_CE_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "NRF24_CE_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 NRF24_CE__PORT == 15 && ((NRF24_CE__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    NRF24_CE_Write(uint8 value);
void    NRF24_CE_SetDriveMode(uint8 mode);
uint8   NRF24_CE_ReadDataReg(void);
uint8   NRF24_CE_Read(void);
void    NRF24_CE_SetInterruptMode(uint16 position, uint16 mode);
uint8   NRF24_CE_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the NRF24_CE_SetDriveMode() function.
     *  @{
     */
        #define NRF24_CE_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define NRF24_CE_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define NRF24_CE_DM_RES_UP          PIN_DM_RES_UP
        #define NRF24_CE_DM_RES_DWN         PIN_DM_RES_DWN
        #define NRF24_CE_DM_OD_LO           PIN_DM_OD_LO
        #define NRF24_CE_DM_OD_HI           PIN_DM_OD_HI
        #define NRF24_CE_DM_STRONG          PIN_DM_STRONG
        #define NRF24_CE_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define NRF24_CE_MASK               NRF24_CE__MASK
#define NRF24_CE_SHIFT              NRF24_CE__SHIFT
#define NRF24_CE_WIDTH              1u

/* Interrupt constants */
#if defined(NRF24_CE__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in NRF24_CE_SetInterruptMode() function.
     *  @{
     */
        #define NRF24_CE_INTR_NONE      (uint16)(0x0000u)
        #define NRF24_CE_INTR_RISING    (uint16)(0x0001u)
        #define NRF24_CE_INTR_FALLING   (uint16)(0x0002u)
        #define NRF24_CE_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define NRF24_CE_INTR_MASK      (0x01u) 
#endif /* (NRF24_CE__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define NRF24_CE_PS                     (* (reg8 *) NRF24_CE__PS)
/* Data Register */
#define NRF24_CE_DR                     (* (reg8 *) NRF24_CE__DR)
/* Port Number */
#define NRF24_CE_PRT_NUM                (* (reg8 *) NRF24_CE__PRT) 
/* Connect to Analog Globals */                                                  
#define NRF24_CE_AG                     (* (reg8 *) NRF24_CE__AG)                       
/* Analog MUX bux enable */
#define NRF24_CE_AMUX                   (* (reg8 *) NRF24_CE__AMUX) 
/* Bidirectional Enable */                                                        
#define NRF24_CE_BIE                    (* (reg8 *) NRF24_CE__BIE)
/* Bit-mask for Aliased Register Access */
#define NRF24_CE_BIT_MASK               (* (reg8 *) NRF24_CE__BIT_MASK)
/* Bypass Enable */
#define NRF24_CE_BYP                    (* (reg8 *) NRF24_CE__BYP)
/* Port wide control signals */                                                   
#define NRF24_CE_CTL                    (* (reg8 *) NRF24_CE__CTL)
/* Drive Modes */
#define NRF24_CE_DM0                    (* (reg8 *) NRF24_CE__DM0) 
#define NRF24_CE_DM1                    (* (reg8 *) NRF24_CE__DM1)
#define NRF24_CE_DM2                    (* (reg8 *) NRF24_CE__DM2) 
/* Input Buffer Disable Override */
#define NRF24_CE_INP_DIS                (* (reg8 *) NRF24_CE__INP_DIS)
/* LCD Common or Segment Drive */
#define NRF24_CE_LCD_COM_SEG            (* (reg8 *) NRF24_CE__LCD_COM_SEG)
/* Enable Segment LCD */
#define NRF24_CE_LCD_EN                 (* (reg8 *) NRF24_CE__LCD_EN)
/* Slew Rate Control */
#define NRF24_CE_SLW                    (* (reg8 *) NRF24_CE__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define NRF24_CE_PRTDSI__CAPS_SEL       (* (reg8 *) NRF24_CE__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define NRF24_CE_PRTDSI__DBL_SYNC_IN    (* (reg8 *) NRF24_CE__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define NRF24_CE_PRTDSI__OE_SEL0        (* (reg8 *) NRF24_CE__PRTDSI__OE_SEL0) 
#define NRF24_CE_PRTDSI__OE_SEL1        (* (reg8 *) NRF24_CE__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define NRF24_CE_PRTDSI__OUT_SEL0       (* (reg8 *) NRF24_CE__PRTDSI__OUT_SEL0) 
#define NRF24_CE_PRTDSI__OUT_SEL1       (* (reg8 *) NRF24_CE__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define NRF24_CE_PRTDSI__SYNC_OUT       (* (reg8 *) NRF24_CE__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(NRF24_CE__SIO_CFG)
    #define NRF24_CE_SIO_HYST_EN        (* (reg8 *) NRF24_CE__SIO_HYST_EN)
    #define NRF24_CE_SIO_REG_HIFREQ     (* (reg8 *) NRF24_CE__SIO_REG_HIFREQ)
    #define NRF24_CE_SIO_CFG            (* (reg8 *) NRF24_CE__SIO_CFG)
    #define NRF24_CE_SIO_DIFF           (* (reg8 *) NRF24_CE__SIO_DIFF)
#endif /* (NRF24_CE__SIO_CFG) */

/* Interrupt Registers */
#if defined(NRF24_CE__INTSTAT)
    #define NRF24_CE_INTSTAT            (* (reg8 *) NRF24_CE__INTSTAT)
    #define NRF24_CE_SNAP               (* (reg8 *) NRF24_CE__SNAP)
    
	#define NRF24_CE_0_INTTYPE_REG 		(* (reg8 *) NRF24_CE__0__INTTYPE)
#endif /* (NRF24_CE__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_NRF24_CE_H */


/* [] END OF FILE */
